package passwordvalidator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author iuabd
 */
public class PasswordValidatorTest {
    
    public PasswordValidatorTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of isValidPasswordAll method, of class PasswordValidator.
     */
    @Test
    public void testIsValidPasswordAll() {
        System.out.println("isValidPasswordAll");
        String password = "He1l@world";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPasswordAll(password);
        assertEquals(expResult, result);
        
    }
    
    @Test
    public void testIsValidPasswordDigit() {
        System.out.println("isValidPasswordAll");
        String password = "He1l@world";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPasswordAll(password);
        assertEquals(expResult, result);
        
    }
    @Test
    public void testIsValidPasswordMinLength() {
        System.out.println("isValidPasswordAll");
        String password = "He1l@wor";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPasswordAll(password);
        assertEquals(expResult, result);
        
    }
    @Test
    public void testIsValidPasswordSpecialChar() {
        System.out.println("isValidPasswordAll");
        String password = "He1l";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPasswordAll(password);
        assertEquals(expResult, result);
        
    }
    @Test
    public void testIsValidPasswordCapitalLetter() {
        System.out.println("isValidPasswordAll");
        String password = "He1l@world";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPasswordAll(password);
        assertEquals(expResult, result);
        
    }
    
    
}
