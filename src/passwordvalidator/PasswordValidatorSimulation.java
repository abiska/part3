package passwordvalidator;

/**
 *
 * @author iuabd
 */
public class PasswordValidatorSimulation {

    public static void main(String[]args){
        
        //pass
        String password = "He1l@world";
        String boundaryInPassword = "He1l@wor";
        
        //fail
        String badpassword = "He1l";
        String boundaryOutPassword = "He1loworld";
        
        //array used in for loop
        String[]passwords = {password, boundaryInPassword, badpassword, boundaryOutPassword};
        
        //for loop, prints the validation results
        for(int i=0;i<4;i++){
            if(!PasswordValidator.isValidPasswordAll(passwords[i]))
            {
               System.out.println("Password " + passwords[i] + " is not valid");
            }
            else
            {
               System.out.println("Password " + passwords[i] + " is valid");
            }
        }
    } 
}
