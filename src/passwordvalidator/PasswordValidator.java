package passwordvalidator;


import java.util.Scanner;
import java.util.regex.Pattern;


/**
 *
 * @author iuabd
 */


public class PasswordValidator { 
    
    //regex
    static final String REGALL = "^(?=.*\\d)(?=.*[@#$%^&+=])(?=.*[a-z])(?=.*[A-Z]).{8,10}$";
    static final Pattern PATTERNALL = Pattern.compile(REGALL);

    //method(s)
    private static boolean checkAll(String password){
        return PATTERNALL.matcher(password).matches();
    }
    
    public static boolean isValidPasswordAll(String password)
    {
       return checkAll(password);
    }
               
}